const { app, BrowserWindow, ipcMain, dialog } = require("electron")

function createWindow() {
    const win = new BrowserWindow({
        //icon: "assets/icons/icon.png",
        width: 1600,
        height: 1024,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true
        }
    })

    win.loadFile("./dist/index.html")
    //win.webContents.openDevTools()
}  

// Window 2
ipcMain.on('window2', function(event, file, title) {
    win2 = new BrowserWindow({width:1000, height: 900, modal: true,
        //icon: "assets/icon.png",
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
      }
    })
    
    win2.loadFile(file)
    //win2.removeMenu()
    win2.title = title
})

// Window 3
ipcMain.on('window3', function(event, title) {
    win3 = new BrowserWindow({width:1000, height: 900, modal: true,
        //icon: "assets/icon.png",
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
      }
    })
    
    win3.loadFile("./dist/ma_anlegen.html")
    //win3.removeMenu()
    win3.title = title
})

app.whenReady().then(createWindow)

app.on("window-all-closed", () => {
    app.quit()
})

app.on("activate", () => {
    if(BrowserWindow.getAllWindows().length == 0) {
        createWindow()
    }
})