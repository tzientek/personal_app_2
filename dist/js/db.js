const mysql = require('mysql');

var connection = mysql.createConnection({
    host     : '172.25.1.39',
    user     : 'electron',
    password : 'electronapp',
    database : 'personal'
});

connection.connect(function(err) {
    if (err) throw err;
});

module.exports = connection;