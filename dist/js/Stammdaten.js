const {app, BrowserWindow, ipcRenderer} = require('electron')

/**
 * Öffnen eines neuen BrowserWindow für die Verwaltung der Mitarbeiter
 * über einen ipcRenderer
 */
const maverwalten = document.querySelector("#maverwalten")
maverwalten.addEventListener("click", function () {
    let file = "./dist/ma_verwalten.html";
    let title = "Mitarbeiter verwalten";
    ipcRenderer.send('window2', file, title);
})