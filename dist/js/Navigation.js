export default class Navigation {
    constructor() {
        this._html = this._html_generieren();
        this._toggle = this._toggle_generieren();
    }

    _html_generieren() {
        /**
         * Im Array 'menupunkte' müssen die Einträge für die Side-Navigation eingetragen werden.
         * Im Array 'links' werden die href-Links für einzelnen Menüpunkte in der Navigation hinterlegt.
         * Im Array 'icons' müssen die SVG-Links für die Symbole in der Navigation eingetragen werden.
         */
        let menupunkte = ["Dashboard", "Planung", "Stammdaten", "Account", "Login"];
        
        let login = 1;
        if (login === 1) {
            menupunkte.pop();
            menupunkte.push("Logout");
        }

        let links = ["index.html", "", "stammdaten.html", "", ""];
        let icons = [
            `<svg xmlns="http://www.w3.org/2000/svg" class="w-6 ml-4 h-15" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
            </svg>`,
            `<svg xmlns="http://www.w3.org/2000/svg" class="w-6 ml-4 h-15" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6V4m0 2a2 2 0 100 4m0-4a2 2 0 110 4m-6 8a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4m6 6v10m6-2a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4" />
            </svg>`,
            `<svg xmlns="http://www.w3.org/2000/svg" class="w-6 ml-4 h-15" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 7v10c0 2.21 3.582 4 8 4s8-1.79 8-4V7M4 7c0 2.21 3.582 4 8 4s8-1.79 8-4M4 7c0-2.21 3.582-4 8-4s8 1.79 8 4m0 5c0 2.21-3.582 4-8 4s-8-1.79-8-4" />
            </svg>`,
            `<svg xmlns="http://www.w3.org/2000/svg" class="w-6 ml-4 h-15" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
            </svg>`,
            `<svg xmlns="http://www.w3.org/2000/svg" class="w-6 ml-4 h-15" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1" />
            </svg>`            
        ];

        let navigation = document.createElement("nav");
        navigation.setAttribute("id", "navibar");

        let navdiv = document.createElement("div");
        navdiv.setAttribute("class", "fixed left-0 overflow-x-hidden transition bg-blue-500 border-l-4 border-blue-500 border-solid rounded-r-3xl navigation w-17 top-24 bottom-20");

        let navul = document.createElement("ul");
        navul.setAttribute("class", "absolute top-0 left-0 w-full pt-10 pl-1");

        for (let i = menupunkte.length -1; i >= 0; i--) {
            let navli = document.createElement("li");
            navli.setAttribute("class", "relative w-full list-none list hover:bg-white rounded-l-xl");

            let nava = document.createElement("a");
            nava.setAttribute("class", "relative flex w-full text-white hover:text-black");
            nava.href = links[i];

            let navspan_1 = document.createElement("span");
            navspan_1.setAttribute("class", "relative block h-16 text-center icon min-w-60 leading-70");
            navspan_1.innerHTML = icons[i];

            let navspan_2 = document.createElement("span");
            navspan_2.setAttribute("class", "relative block pl-3 whitespace-normal h-15 title leading-60");
            navspan_2.textContent = menupunkte[i];

            nava.insertAdjacentElement("afterbegin", navspan_2);
            nava.insertAdjacentElement("afterbegin", navspan_1);

            navli.insertAdjacentElement("afterbegin", nava);

            navul.insertAdjacentElement("afterbegin", navli);

            navdiv.insertAdjacentElement("afterbegin", navul);

            navigation.insertAdjacentElement("afterbegin", navdiv);
        }

        return navigation;
    }

    _toggle_generieren() {
        let toggle = document.createElement("div");
        toggle.setAttribute("class", "toggle");

        toggle.innerHTML = `
        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 ion-icon open" fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
        </svg>
        
        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 ion-icon close" fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
        </svg>`;

        return toggle;
    }

    anzeigen() {
        let nav = document.querySelector("#navi");
        if (nav !== null) {
            nav.insertAdjacentElement("afterbegin", this._html);
            nav.insertAdjacentElement("beforeend", this._toggle);
        }
    }
}