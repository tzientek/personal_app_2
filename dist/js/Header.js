export default class Header {
    constructor() {
        this._html = this._html_generieren();
    }

    _html_generieren() {
        let header1 = document.createElement("div");
        header1.setAttribute("class", "h-20 border-black border-b-1 bg-gradient-to-r from-purple-400 via-pink-500 to-red-500");
        //header1.setAttribute("class", "h-20 border-black border-b-1 bg-gradient-to-r from-red-500 via-orange-500 to-white-500");
        let header2 = document.createElement("div");       
        let header3 = document.createElement("div");
        header3.setAttribute("class", "grid grid-cols-3");
        let header4 = document.createElement("h1");
        header4.setAttribute("class", "mt-4 ml-4 text-4xl text-white");
        header4.textContent = "Personal-App";
                
        header3.insertAdjacentElement("afterbegin", header4);
        header2.insertAdjacentElement("afterbegin", header3);
        header1.insertAdjacentElement("afterbegin", header2);

        return header1;
    }

    anzeigen() {
        let body = document.querySelector("#header");
        if (body !== null) {
            body.insertAdjacentElement("afterbegin", this._html);
        }
    }
}