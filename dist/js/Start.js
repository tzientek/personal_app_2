import Header from "./Header.js";
import Navigation from "./Navigation.js";
let path = require('path');
//console.log(__dirname);

export default class Basic {
    constructor() {
        this.Header = new Header();
        this.Navigation = new Navigation();
    }

    start() {
        this.Header.anzeigen();
        this.Navigation.anzeigen();
    }
}