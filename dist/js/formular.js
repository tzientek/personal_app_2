const db = require("/Coding/Electron/Personal-App/dist/js/db");

export default class Formular {
    constructor() {
        this._formular = this.form();
    }

    _formulardaten_holen(submit_event) {
        return {
            vorname: submit_event.target.elements.vorname.value,
            nachname: submit_event.target.elements.nachname.value,
            datum: submit_event.target.elements.datum.valueAsDate,
            firma: submit_event.target.elements.firma.value,
            //abteilung: submit_event.target.elements.abteilung.value,
            anmerkungen: submit_event.target.elements.anmerkungen.value
        }  
    }

    _formulardaten_verarbeiten(formulardaten) {
        return {
            vorname: formulardaten.vorname.trim(),
            nachname: formulardaten.nachname.trim(),
            datum: formulardaten.datum,
            firma: formulardaten.firma,
            //abteilung: formulardaten.abteilung,
            anmerkungen: formulardaten.anmerkungen
        }
    }

    _formulardaten_validieren(formulardaten) {
        let fehler = [];
        
        if (formulardaten.vorname === "") {
            fehler.push("Vorname");
        } 

        if (formulardaten.nachname === "") {
            fehler.push("Nachname");
        }

        if (formulardaten.datum === null) {
            fehler.push("Datum");
        } 

        // if (formulardaten.abteilung === null) {
        //     //fehler.push("Datum");
        // } 

        if (formulardaten.firma === null) {
            fehler.push("Firma");
        }

        return fehler;
    }

    _datum_aktualisieren() {
        let datums_input = document.querySelector("#datum");
        if (datums_input !== null) {
            datums_input.valueAsDate = new Date();
        }
    }

    _absenden_event_hinzufuegen(form) {
        form.querySelector("#form-container");
        form.addEventListener("submit", e => {
        e.preventDefault();
        // Formulardaten holen und verarbeiten
        let formulardaten = this._formulardaten_verarbeiten(this._formulardaten_holen(e));
        // Formulardaten validieren und eintragen wenn OK
        let formular_fehler = this._formulardaten_validieren(formulardaten);
        if (formular_fehler.length === 0) {
            if (sessionStorage.getItem("id") === null) {
                this.eintrag_hinzufuegen(formulardaten);
                let bestehende_fehlerbox = document.querySelector(".fehlerbox");
                if (bestehende_fehlerbox !== null) {
                    bestehende_fehlerbox.remove();
                }
                e.target.reset();
                e.target.vorname.focus();
                this._datum_aktualisieren();
                // } else {
                // // let fehler = new Fehlerbox("Folgende Felder wurden nicht korrekt ausgefüllt:", formular_fehler);
                // // fehler.anzeigen();
                // }
            }
            else {
                this.eintrag_bearbeiten(formulardaten);
            }
        }});
    }

    eintrag_hinzufuegen(formulardaten) {
        console.log("Hinzufügen");
        let date = formulardaten.datum.toLocaleDateString("de-DE", {
            year: "numeric",
            month: "2-digit",
            day: "2-digit"
        }); 
        db.query(`INSERT INTO mitarbeiter (vorname, nachname, firma_id, abteilung, edatum, kommentar) VALUES ("${formulardaten.vorname}", "${formulardaten.nachname}", "${formulardaten.firma}", "", "${date}", "${formulardaten.anmerkungen}")`), function(err, result, fields){
            if (err)
            console.log("Error inserting : %s ",err );
        };
    }

    eintrag_bearbeiten(formulardaten) {
        let date = formulardaten.datum.toLocaleDateString("de-DE", {
            year: "numeric",
            month: "2-digit",
            day: "2-digit"
        }); 
        db.query(`UPDATE mitarbeiter SET vorname = "${formulardaten.vorname}", nachname = "${formulardaten.nachname}", firma_id = "${formulardaten.firma}", abteilung = "", edatum = "${date}", kommentar = "${formulardaten.anmerkungen}" WHERE id = "${sessionStorage.getItem("id")}"`), function(err, result, fields){
            if (err);
            console.log("Error updating : %s ",err );
        };
        window.history.go(-1);
    }

    eintrag_loeschen(id) {
        db.query(`DELETE FROM mitarbeiter WHERE id = "${id}"`), function(err, result, fields){
            if (err) {
                console.log("Error deleting : %s ",err );
            };
            sessionStorage.removeItem("id");
            window.prompt(`Mitarbeiter mit der ID ${id} wurde gelöscht!`);
            location.reload(forcedReload);
        };
    }

    // _abteilungen_einlesen() {
    //     console.log("Abteilungen");
    //     db.query(`SELECT * FROM abteilungen`), function(err, result, fields){
    //         if (err) throw err;
    //         console.log("Einlesen");
    //         Object.keys(result).forEach(function(key) {
    //             let resultArray = Object.values(JSON.parse(JSON.stringify(result)))
    //             console.log(resultArray.length);
    //             return resultArray;
    //         });
    //     };
    // }

    form() {
        let form = document.createElement("form");
        form.setAttribute("id", "formular");
        form.setAttribute("action", "#");
        form.setAttribute("method", "get");
        
        let div1 = document.createElement("div");
        div1.setAttribute("class", "max-w-md mt-8 ml-5");

        let div2 = document.createElement("div");
        div2.setAttribute("class", "grid grid-cols-1 gab-6")

        let div3 = document.createElement("div");
        div3.setAttribute("class", "block");

        let label1 = document.createElement("label");
        label1.setAttribute("class", "block");

        let label2 = document.createElement("label");
        label2.setAttribute("class", "block");

        let label3 = document.createElement("label");
        label3.setAttribute("class", "block");

        let label4 = document.createElement("label");
        label4.setAttribute("class", "block");

        let label5 = document.createElement("label");
        label5.setAttribute("class", "block");

        let span1 = document.createElement("span");
        span1.setAttribute("class", "text-gray-700");
        span1.textContent = "Vorname";

        let span2 = document.createElement("span");
        span2.setAttribute("class", "text-gray-700");
        span2.textContent = "Nachname";

        let span3 = document.createElement("span");
        span3.setAttribute("class", "text-gray-700");
        span3.textContent = "Eintrittsdatum";

        let span4 = document.createElement("span");
        span4.setAttribute("class", "text-gray-700");
        span4.textContent = "Firma";

        let span5 = document.createElement("span");
        span5.setAttribute("class", "text-gray-700");
        span5.textContent = "Anmerkungen / Kommentare";

        let input_text1 = document.createElement("input");
        input_text1.setAttribute("class", "mt-0 block w-full px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black");
        input_text1.setAttribute("type", "text");
        input_text1.setAttribute("id", "vorname");

        let input_text2 = document.createElement("input");
        input_text2.setAttribute("class", "mt-0 block w-full px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black");
        input_text2.setAttribute("type", "text");
        input_text2.setAttribute("id", "nachname");

        let input_date = document.createElement("input");
        input_date.setAttribute("class", "mt-0 block w-full px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black");
        input_date.setAttribute("type", "date");
        input_date.setAttribute("id", "datum");

        let input_select = document.createElement("select");
        input_select.setAttribute("class", "block w-full mt-0 px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black");
        input_select.setAttribute("type", "text");
        input_select.setAttribute("id", "firma");

        let input_tarea = document.createElement("textarea");
        input_tarea.setAttribute("class", "mt-0 block w-full px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black");
        input_tarea.setAttribute("id", "anmerkungen");

        let button = document.createElement("button");
        button.setAttribute("type", "submit");
        button.setAttribute("class", "block w-full p-2 mt-4 ml-1 text-white bg-blue-500 rounded-md min-w-180 hover:bg-red-400");
        button.setAttribute("form", "formular");
        button.textContent = "Speichern";

        let delete_button = document.createElement("button");
        delete_button.setAttribute("type", "button");
        delete_button.setAttribute("class", "block w-full p-2 mt-4 ml-1 text-white bg-blue-500 rounded-md min-w-180 hover:bg-red-400");
        delete_button.textContent = 'Löschen';

        db.connect();
        let result = db.query('SELECT * FROM mitarbeiter');
        console.log(result);
        db.query('SELECT * FROM abteilungen'), function(err, result, fields){           
            if (err) throw err;
            console.log("Einlesen");
            Object.keys(result).forEach(function(key) {
                let resultArray = Object.values(JSON.parse(JSON.stringify(result)))
                console.log(resultArray.length);
                return resultArray;
            });
        };

        let resultArray;
        function getOptions() {
            db.query('SELECT name FROM firmen', function(err, result, fields){
                if (err) throw err;
                Object.entries(result).forEach(function(key) {
                resultArray = Object.values(JSON.parse(JSON.stringify(result)))
            });
            setOptions(resultArray);
            })
        }
        getOptions();

        function setOptions(resultArray) {
            let optvalue = [];
            for (let i in resultArray) {
                let option = document.createElement("option");    
                let split = JSON.stringify(resultArray[i]).split(":")
                option.text = split[1];
                option.text = option.text.replace("{", "");
                option.text = option.text.replace("}", "");
                option.text = option.text.replace(/"/g, "");
                input_select.appendChild(option);
            }
            
            if (sessionStorage.getItem("id") !== null) {
                div3.insertAdjacentElement("afterbegin", delete_button);
            }
            div3.insertAdjacentElement("afterbegin", button);
            label5.insertAdjacentElement("afterbegin", input_tarea);
            label5.insertAdjacentElement("afterbegin", span5);
            label4.insertAdjacentElement("afterbegin", input_select);
            label4.insertAdjacentElement("afterbegin", span4);
            label3.insertAdjacentElement("afterbegin", input_date);
            label3.insertAdjacentElement("afterbegin", span3);
            label2.insertAdjacentElement("afterbegin", input_text2);
            label2.insertAdjacentElement("afterbegin", span2);
            label1.insertAdjacentElement("afterbegin", input_text1);
            label1.insertAdjacentElement("afterbegin", span1);
            div2.insertAdjacentElement("afterbegin", div3);
            div2.insertAdjacentElement("afterbegin", label5);
            div2.insertAdjacentElement("afterbegin", label4);
            div2.insertAdjacentElement("afterbegin", label3);
            div2.insertAdjacentElement("afterbegin", label2);
            div2.insertAdjacentElement("afterbegin", label1);
            div1.insertAdjacentElement("afterbegin", div2);
            form.insertAdjacentElement("afterbegin", div1);    
        }

        if (sessionStorage.getItem("id") !== null) {
            input_tarea.textContent = sessionStorage.getItem("kommentar");
            input_select.option = sessionStorage.getItem("firma");
            //input_date.valueAsDate = Date(sessionStorage.getItem("edatum"));
            input_text2.value = sessionStorage.getItem("nachname");
            input_text1.value = sessionStorage.getItem("vorname");
        };

        if (delete_button.addEventListener("click", e => {
            let id = sessionStorage.getItem("id");
            this.eintrag_loeschen(id);
        }));

        //this._abteilungen_einlesen();
        this._absenden_event_hinzufuegen(form);
        return form;
    }

    anzeigen() {
        let form = document.querySelector("#form-container");
        if (form !== null) {
            form.insertAdjacentElement("afterbegin", this._formular);
        }
    }
}