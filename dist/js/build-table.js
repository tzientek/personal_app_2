const { app } = require("electron/main");
const path = require("path")
const pfad = path.join(__dirname+"\\data\\", "")
const db = require("/Coding/Electron/Personal-App/dist/js/db")

let resultArray;

function getMitarbeiter() {    
    db.query('SELECT * FROM mitarbeiter', function(err, result, fields){
      if (err) throw err;
      Object.keys(result).forEach(function(key) {
        //let row = result[key];
        resultArray = Object.values(JSON.parse(JSON.stringify(result)))
      });
      let table = document.querySelector("table");

      function generateTable(table) {
        for (let element of resultArray) {
          let row = table.insertRow();
          row.setAttribute("id", element.id);
          for (let key in element) {
            let cell = row.insertCell();
            cell.setAttribute("class", "bg-gray-100 p-2 w-auto hover:bg-red-200");
            let text = document.createTextNode(element[key]);
            cell.appendChild(text);    
          }
          let cell_options = row.insertCell();
          cell_options.setAttribute("class", "bg-gray-100 p-2 w-auto hover:bg-red-200");
          let edit_icon = `<svg xmlns="http://www.w3.org/2000/svg" id="edit" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" /></svg>`
          let icons = `<div class="grid grid-cols-2"><a href="ma_bearbeiten.html?id=${element.id}" id="edit${element.id}">${edit_icon}</a></div>`;
          cell_options.insertAdjacentHTML("afterbegin", icons);

          document.getElementById("edit"+element.id).onmousedown = function() {
            sessionStore(sessionStorage.setItem("id", element.id));
            sessionStore(sessionStorage.setItem("vorname", element.vorname));
            sessionStore(sessionStorage.setItem("nachname", element.nachname));
            sessionStore(sessionStorage.setItem("abteilung", element.abteilung));
            sessionStore(sessionStorage.setItem("edatum", element.edatum));
            sessionStore(sessionStorage.setItem("firma", element.firma_id));
            sessionStore(sessionStorage.setItem("kommentar", element.kommentar));            
          }
        }
      }
      generateTable(table);
      
      function generateTableHead(table) {
        table = document.querySelector("table");
        let thead = table.createTHead();
        let row = thead.insertRow();
        let tablehead = ["ID", "Vorname", "Nachname", "Firma", "Abteilung", "Eintritt", "Anmerkungen", "Optionen"];
        
          for (let e of Object.values(tablehead)) {
          let th = document.createElement("th");
          let th2 = document.createElement("th");
          th.setAttribute("class", "static p-2 mt-5 mr-32 w-1/12 md:w-auto space-x-3 ml-60 bg-blue-500 transition text-white hover:bg-red-200");
          th2.setAttribute("class", "static p-2 mt-5 mr-32 w-1/12 md:w-auto space-x-3 ml-60 bg-blue-500 transition text-white hover:bg-red-200");
          let text = document.createTextNode(e);
          th.appendChild(text);
          row.appendChild(th);
          }
        }
      
      generateTableHead(table);
      });

      function sessionStore(id) {
          sessionStorage.getItem("id", id);
      }
  }
    
    getMitarbeiter();