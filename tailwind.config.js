module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      spacing: {
        15: '60px',
        17: '70px',
        300: '300px'
      },
      minWidth: {
        60: '60px',
        120: '120px',
        180: '180px',
      },
      lineHeight: {
        60: '60px',
        70: '70px'
      }
    },
  },
  variants: {
    extend: {
  
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
